import turtle

def poly(t,x,y):
	t.goto(0,0)
	
	t.seth(0)
	t.forward(100)
	t.pencolor('#FF0000')
	t.fillcolor('#FF0000')
	t.begin_fill()
	for n in range (0,4):
		t.forward(50)
		t.left(90)
	t.end_fill()

	turtle.update()

def spinner(t,x,y,):
	colorlist = ["#f8f8f8","#e8e8e8" ,"#d8d8d8" ,"#b8b8b8" ,
				"#585858" ,"#383838" ,"#282828" , "#181818" ,
				"#ab4642" ,	"#dc9656" ,"#f79a0e" ,"#538947" ,
				"#4b8093" ,"#7cafc2" ,"#96609e" ,"#a16946" ]
	print(t,x,y)
	t.width(10)
	for n in range(0,375,45):
		t.penup()
		t.goto(x,y)
		t.pendown()
		t.seth(n)
		t.fd(70)
		

def main():
	turtle.TurtleScreen._RUNNING = True
	turtle.screensize(canvwidth=1000, canvheight=1000, bg=None)
	x = -30; y = 0
	w = turtle.Screen()
	w.clear()
	w.bgcolor("#ffffff")
	t = turtle.Turtle()
	#turtle.tracer(0, 0)
	#turtle.tracer(0, 0)
	
	spinner(t,100,90)
	spinner(t,10,10)
	w.exitonclick()
	
if __name__ == '__main__':
	main()
