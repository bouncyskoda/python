import turtle
# main function
def main():
	screen = turtle.Screen()
	t = turtle.Turtle()
	#t.tracer(0, 0)

	dist = 2
	for i in range(1000000000000000):
		t.fd(dist)
		t.rt(45)
		dist += 0.25
		print(dist)
	#turtle.update()
	screen.exitonclick()	
	

if __name__ == "__main__":
		main()

'''
https://docs.python.org/2/library/turtle.html#turtle.delay

screen.tracer(8, 25)
>>> dist = 2
>>> for i in range(200):
...     fd(dist)
...     rt(90)
...     dist += 2

'''
