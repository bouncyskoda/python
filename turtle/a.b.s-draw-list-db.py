import turtle

def poly(t,x,y,size,side,color):

	t.pencolor('#000000')
	t.fillcolor(color)
	t.begin_fill()
	for n in range (0,4):
		t.forward(size)
		t.left(360/side)
	t.end_fill()

	turtle.update()

def matrix(t,x,y,):
	# color list
	cl = ["#282828" , "#181818" ,"#ab4642" ,	"#dc9656" ,"#f79a0e" ,
			"#538947" ,	"#4b8093" ,"#7cafc2" ,"#96609e" ,"#a16946" ]
	# design design list
	mlist = ["01234567890123456789012345678",
			 "12345678901234567890123456789",
			 "23456789012345678901234567890",
			 "34567890123456789012345678901",
			 "45678901234567890123456789012",
			 "56789012345678901234567890123",
			 "67890123456789012345678901234",
			 "78901234567890123456789012345",
			 "89012345678901234567890123456",
			 "90123456789012345678901234567"]
	print(t,x,y)
	t.width(1)
	for r in range(0,10):
		for c in range(0,29):
			colorstring = mlist[r][c]
			colorint = int(colorstring)
			color = cl[colorint]
			print(c*20,r*20)
			print(" color ",color,end="")
			t.penup()
			t.goto(c*20,r*20)
			t.pendown()
			poly(t,r,c,20,4,color)
			


def main():
	turtle.TurtleScreen._RUNNING = True
	turtle.screensize(canvwidth=1000, canvheight=1000, bg=None)
	x = -200; y = 200
	w = turtle.Screen()
	w.clear()
	w.bgcolor("#925CD3")
	t = turtle.Turtle()

	#turtle.tracer(0, 0)
	
	matrix(t,x,y)
	
	w.exitonclick()
	
if __name__ == '__main__':
	main()

'''
cl = ["#f8f8f8","#e8e8e8" ,"#d8d8d8" ,"#b8b8b8" ,
				"#585858" ,"#383838" ,"#282828" , "#181818" ,
				"#ab4642" ,	"#dc9656" ,"#f79a0e" ,"#538947" ,
				"#4b8093" ,"#7cafc2" ,"#96609e" ,"#a16946" ]

'''
